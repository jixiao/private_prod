import yaml
import argparse
import logging
import subprocess

logger = logging.getLogger("step")
# create console handler and set level to debug
ch = logging.StreamHandler()

parser = argparse.ArgumentParser(description="manual to this script")
parser.add_argument(
    "-i",
    "--input",
    help="input yaml file for cmssw version and cmd for each step",
    default="step_2022EE_nanov12.yml",
)
parser.add_argument(
    "-o",
    "--output",
    help="output setting script",
    default="run_2022EE_nanov12.sh",
)
parser.add_argument(
    "--debug",
    action="store_true",
    default=False,
)

args = parser.parse_args()
if args.debug:
    logger.setLevel(getattr(logging, "DEBUG"))
    ch.setLevel(getattr(logging, "DEBUG"))
else:
    logger.setLevel(getattr(logging, "INFO"))
    ch.setLevel(getattr(logging, "INFO"))
# create formatter
formatter = logging.Formatter("%(message)s")
# add formatter to ch
ch.setFormatter(formatter)
logger.addHandler(ch)


def step_modifier(istep, cmd):
    # step1 => step2_1 => step2_2 => step3 => step4

    new_cmd = cmd
    if istep == "step1":
        logger.debug(f"Old command: {cmd}")
        cmd_split = cmd.split()
        # rename init fragment
        idx = cmd_split.index("cmsDriver.py")
        cmd_split[idx + 1] = "Configuration/GenProduction/python/fragment.py"
        # output script name
        idx = cmd_split.index("--python_filename")
        cmd_split[idx + 1] = "step1.py"
        # output file name
        idx = cmd_split.index("--fileout")
        cmd_split[idx + 1] = "file:step1.root"
        # change customise_commands
        idx = cmd_split.index("--customise_commands")
        cmd_split[
            idx + 1
        ] = 'process.source.numberEventsInLuminosityBlock="cms.untracked.uint32(333)"'
        # keep events as a placeholder
        idx = cmd_split.index("-n")
        cmd_split[idx + 1] = "$4"
        # add customize randomseed
        cmd_split.append("--customise")
        cmd_split.append("Configuration/GenProduction/randomizeSeeds.randomizeSeeds")
        new_cmd = " ".join(cmd_split)
        logger.debug(f"New command: {new_cmd}")
    elif istep == "step2_1":
        logger.debug(f"Old command: {cmd}")
        cmd_split = cmd.split()
        # output script name
        idx = cmd_split.index("--python_filename")
        cmd_split[idx + 1] = "step2_1.py"
        # output file name
        idx = cmd_split.index("--fileout")
        cmd_split[idx + 1] = "file:step2_1.root"
        # input file name
        idx = cmd_split.index("--filein")
        cmd_split[idx + 1] = "file:step1.root"
        # fix events to -1
        idx = cmd_split.index("-n")
        cmd_split[idx + 1] = "-1"
        new_cmd = " ".join(cmd_split)
        logger.debug(f"New command: {new_cmd}")
    elif istep == "step2_2":
        logger.debug(f"Old command: {cmd}")
        cmd_split = cmd.split()
        # output script name
        idx = cmd_split.index("--python_filename")
        cmd_split[idx + 1] = "step2_2.py"
        # output file name
        idx = cmd_split.index("--fileout")
        cmd_split[idx + 1] = "file:step2_2.root"
        # input file name
        idx = cmd_split.index("--filein")
        cmd_split[idx + 1] = "file:step2_1.root"
        # fix events to -1
        idx = cmd_split.index("-n")
        cmd_split[idx + 1] = "-1"
        new_cmd = " ".join(cmd_split)
        logger.debug(f"New command: {new_cmd}")
    elif istep == "step3":
        logger.debug(f"Old command: {cmd}")
        cmd_split = cmd.split()
        # output script name
        idx = cmd_split.index("--python_filename")
        cmd_split[idx + 1] = "step3.py"
        # output file name
        idx = cmd_split.index("--fileout")
        cmd_split[idx + 1] = "file:step3.root"
        # input file name
        idx = cmd_split.index("--filein")
        cmd_split[idx + 1] = "file:step2_2.root"
        # fix events to -1
        idx = cmd_split.index("-n")
        cmd_split[idx + 1] = "-1"
        new_cmd = " ".join(cmd_split)
        logger.debug(f"New command: {new_cmd}")
    elif istep == "step4":
        logger.debug(f"Old command: {cmd}")
        cmd_split = cmd.split()
        # output script name
        idx = cmd_split.index("--python_filename")
        cmd_split[idx + 1] = "step4.py"
        # output file name
        idx = cmd_split.index("--fileout")
        cmd_split[idx + 1] = "file:step4.root"
        # input file name
        idx = cmd_split.index("--filein")
        cmd_split[idx + 1] = "file:step3.root"
        # fix events to -1
        idx = cmd_split.index("-n")
        cmd_split[idx + 1] = "-1"
        # change NANOEDMAODSIM to NANOAODSIM
        idx = cmd_split.index("--eventcontent")
        cmd_split[idx + 1] = "NANOAODSIM"
        new_cmd = " ".join(cmd_split)
        logger.debug(f"New command: {new_cmd}")
    return new_cmd


def generate_run_script(output, steps):
    to_run = """
#!/bin/bash

echo "Starting job on " `date` #Date/time of start of job
echo "Running on: `uname -a`" #Condor job is running on this node
echo "System software: `cat /etc/redhat-release`" #Operating System on that node
echo "Init path: `pwd`"
echo "[Process] "$1
echo "[Gridpack] "$2
echo "[Fragment] "$3
echo "[NEvents] "$4

# set scram_arch
# export SYSTEM_RELEASE=`cat /etc/redhat-release`
# echo $SYSTEM_RELEASE
#
# if [[ $SYSTEM_RELEASE == *"release 7"* ]]; then
#     SCRAM_ARCH=slc7_amd64_gcc10
# elif [[ $SYSTEM_RELEASE == *"release 8"* ]]; then
#     SCRAM_ARCH=el8_amd64_gcc10
# elif [[ $SYSTEM_RELEASE == *"release 9"* ]]; then
#     SCRAM_ARCH=el9_amd64_gcc11
# else
#     echo "No default scram_arch for current OS!"
# fi

# set environment
INIT_PATH=$PWD
echo "[Init path] "$INIT_PATH
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH={9}
if [ -r {0}/src ] ; then
  echo release {0} already exists
else
  scram p CMSSW {0}
fi
cd {0}/src
eval `scram runtime -sh`

cd -
# get gridpack and fragment
gridpack_name=$2
xrdcp root://eosuser.cern.ch//eos/user/j/jixiao/www/hgg/gridpacks/$gridpack_name .
fragment_name=$3
xrdcp root://eosuser.cern.ch//eos/user/j/jixiao/www/hgg/genfragments/$fragment_name .
fragment_header="fragment_header.py"
xrdcp root://eosuser.cern.ch//eos/user/j/jixiao/www/hgg/template/$fragment_header .
cd -

# Download fragment from McM
mkdir -p Configuration/GenProduction/python
xrdcp root://eosuser.cern.ch//eos/user/j/jixiao/www/genval/scripts/randomizeSeeds.py Configuration/GenProduction/python/randomizeSeeds.py
# define the fragment
cat $INIT_PATH/$fragment_header > Configuration/GenProduction/python/fragment.py
cat $INIT_PATH/$fragment_name >> Configuration/GenProduction/python/fragment.py
echo "" >> Configuration/GenProduction/python/fragment.py
echo "" >> Configuration/GenProduction/python/fragment.py
echo "" >> Configuration/GenProduction/python/fragment.py
echo "ProductionFilterSequence = cms.Sequence(generator)" >> Configuration/GenProduction/python/fragment.py
echo "---------------------- [Step1 Fragment] ----------------------"
cat Configuration/GenProduction/python/fragment.py
echo "---------------------- [Step1 Fragment] ----------------------"
scram b

cd ../..
# step 1
{1}
# change the gridpack
sed -i "s/^.*tarball.tar.xz.*$/    args = cms.vstring(\'..\/..\/$gridpack_name\'),/" -i step1.py
echo "---------------------- [Step1 Script] ----------------------"
cat step1.py
echo "---------------------- [Step1 Script] ----------------------"

echo "[`date`] >>>>>>>>> [wmLHEGS] step1.py"
cmsRun step1.py
echo "[`date`] >>>>>>>>> [wmLHEGS] generate step1.root"
rm $gridpack_name

# step 2
export SCRAM_ARCH={10}
if [ -r {2}/src ] ; then
  echo release {2} already exists
else
  scram p CMSSW {2}
fi
cd {2}/src
eval `scram runtime -sh`
scram b
cd ../..

{3}
echo "[`date`] >>>>>>>>> [Premix0] step2_1.py"
cmsRun step2_1.py
echo "[`date`] >>>>>>>>> [Premix0] generate step2_1.root"
rm step1.root
{4}
echo "[`date`] >>>>>>>>> [AOD] step2_2.py"
cmsRun step2_2.py
echo "[`date`] >>>>>>>>> [AOD] generate step2_2.root"
rm step2_1.root

# step 3
export SCRAM_ARCH={12}
if [ -r {5}/src ] ; then
  echo release {5} already exists
else
  scram p CMSSW {5}
fi
cd {5}/src
eval `scram runtime -sh`
scram b
cd ../..

{6}
echo "[`date`] >>>>>>>>> [MiniAOD] step3.py"
cmsRun step3.py
echo "[`date`] >>>>>>>>> [MiniAOD] generate step3.root"
rm step2_2.root

# step 4
export SCRAM_ARCH={13}
if [ -r {7}/src ] ; then
  echo release {7} already exists
else
  scram p CMSSW {7}
fi
cd {7}/src
eval `scram runtime -sh`
scram b
cd ../..

{8}
echo "[`date`] >>>>>>>>> [NanoAOD] step4.py"
cmsRun step4.py
echo "[`date`] >>>>>>>>> [NanoAOD] generate step4.root"
rm step3.root
# clean
rm -rf $gridpack_name *py *pyc CMSSW*

    """.format(
        steps["step1"]["cmssw"],
        steps["step1"]["cmd"],
        steps["step2_1"]["cmssw"],
        steps["step2_1"]["cmd"],
        steps["step2_2"]["cmd"],
        steps["step3"]["cmssw"],
        steps["step3"]["cmd"],
        steps["step4"]["cmssw"],
        steps["step4"]["cmd"],
        steps["step1"]["scram_arch"],
        steps["step2_1"]["scram_arch"],
        steps["step2_2"]["scram_arch"],
        steps["step3"]["scram_arch"],
        steps["step4"]["scram_arch"],
    )
    # print(to_run)
    with open(output, "w") as f:
        f.write(to_run)
    subprocess.call(['chmod', '+x', output])

    return


if __name__ == "__main__":
    with open(args.input, "r") as f:
        steps = yaml.safe_load(f)
    for i in steps:
        # print(f"[step_producer] {i}")
        steps[i]["cmd"] = step_modifier(i, steps[i]["cmd"])
        # print(steps[i]["cmd"])
    generate_run_script(args.output, steps)
