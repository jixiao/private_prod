#!/usr/bin/python3

import argparse
from copy import deepcopy
import numpy as np
from pathlib import Path
import logging


parser = argparse.ArgumentParser("Generator validation scripts")

parser.add_argument("-i","--input",default=None,required=True,type=str,help="folder of input log files")
parser.add_argument("-p","--pattern",default="_[0-9]*_[0-9]*.err",type=str,help="match pattern of the postfix")
parser.add_argument("-d", "--dataset", \
                    default=None, required=True, type=str, help="dataset name")
parser.add_argument("-n", "--nproc", \
                    default=None, required=True, type=int, help="number of processes")
parser.add_argument("-v","--verbose",default="INFO",type=str,help="verbose level",choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"])
args = parser.parse_args()

FORMAT = '%(levelname)-8s %(message)s'
logging.basicConfig(level=args.verbose,format=FORMAT)

def store_info(log_list,nproc):
    """Read genXsecAnalyzer infomation from log file"""
    xsecs = dict()
    nevts = dict()
    for i in range(0,nproc+1):
        xsecs[f"proc-{i}"] = []
        nevts[f"proc-{i}"] = []
    xsecs["total"] = []
    nevts["total"] = []
    bad_log=deepcopy(log_list)
    good_log=[]
    for ilog in log_list:
        try:
            with open(ilog, 'r') as f:
                for l in f.readlines():
                    if "Before matching: total cross section = " in l: break
                    if not "+/-" in l: continue
                    else:
                        try:
                            if int(l[0]) in range(nproc+1):
                                key = f"proc-{int(l[0])}"
                                info_string = l.split("\t")
                                # xsec_before, xsec_match
                                xsecs[key].append((info_string[2], info_string[10]))
                                # npass_pos, npass_neg, ntotal_pos, ntotal_neg
                                nevts[key].append((int(info_string[5]), int(info_string[6]), int(info_string[8]), int(info_string[9])))
                        except:
                            if l[:5] == "Total":
                                info_string = l.split("\t")
                                xsecs["total"].append((info_string[2], info_string[10]))
                                nevts["total"].append((int(info_string[5]), int(info_string[6]), int(info_string[8]), int(info_string[9])))
                                bad_log.remove(ilog)
                                good_log.append(ilog)
                                # print(ilog,(int(info_string[5]), int(info_string[6]), int(info_string[8]), int(info_string[9])))
                                break
        except:
            logging.warning(f"[genXsecAnalyzer] Could not open {ilog}, skip!!!")
    logging.info(f"[genXsecAnalyzer] good log file: {len(good_log)} - bad log file: {len(bad_log)}")
    # print(bad_log)
    # print(good_log)
    return xsecs, nevts

def eval_xsec_and_err(xsecs, errors):
    weighted_sum, error = 0., 0.
    for i in range(len(xsecs)):
        xsec, err = xsecs[i], errors[i]
        weighted_sum += xsec/pow(err, 2)
        error += 1./pow(err, 2)
    xsec = weighted_sum / error
    error = 1./np.sqrt(error)
    return (xsec, error)

def eval_matching_eff(xsecs, nevts):
    """Caculate matching efficiencies and xsec & err after matching"""
    """Based on https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/Core/plugins/GenXSecAnalyzer.cc#L411-L474"""
    logging.info(f"\t{args.dataset}\txsec_before[pb]\txsec_match[pb]\taccepted[%]")
    for key in xsecs.keys():
	    # get xsecs & err before matching first
        # why all the xsec errs same before matching?
        xsec = xsecs[key][0][0].split("+/-")
        xsec_before, xsec_before_err = float(xsec[0]), float(xsec[1])
        npass_pos,npass_neg,ntotal_pos,ntotal_neg = 0,0 ,0, 0
        for (np_pos, np_neg, nt_pos, nt_neg) in nevts[key]:
            npass_pos += np_pos
            npass_neg += np_neg
            ntotal_pos += nt_pos
            ntotal_neg += nt_neg
        npass = npass_pos-npass_neg; ntotal = ntotal_pos-ntotal_neg;
        fracAcc = npass/ntotal

        effp = npass_pos/ntotal_pos             if ntotal_pos != 0 else 0.
        effp_err2 = (1-effp)*effp/ntotal_pos    if ntotal_pos != 0 else 0.
        effn = npass_neg/ntotal_neg             if ntotal_neg != 0 else 0.
        effn_err2 = (1-effn)*effn/ntotal_neg    if ntotal_neg != 0 else 0.
        efferr2 = (pow(ntotal_pos, 2)*effp_err2+pow(ntotal_neg, 2)*effn_err2)/pow(ntotal, 2)
        delta2Veto = efferr2/pow(fracAcc, 2)
        delta2Sum = delta2Veto + pow(xsec_before_err, 2)/pow(xsec_before, 2)
        xsec_match, xsec_match_err = xsec_before*fracAcc, xsec_before*fracAcc*np.sqrt(delta2Sum)
        logging.info(f"\t[{key}]\t{xsec_before:.2f} +/- {xsec_before_err:.2f}\t{xsec_match:.2f} +/- {xsec_match_err:.2f}\t{(fracAcc)*100:.2f}")

def get_log_file(log_path,dataset,pattern):
    log_path=Path(log_path).glob(f'{dataset}{pattern}')
    log_list=[]
    for i in log_path:
        log_list.append(i.absolute().as_posix())
    return log_list

if __name__ == "__main__":
    logging.info(f"""
========= genXsecAnalyzer Options =========
    input: {args.input}
    dataset: {args.dataset}
    pattern: {args.pattern}
    nprocess: {args.nproc}
========= genXsecAnalyzer Options =========
        """
    )

    log_list=get_log_file(args.input,args.dataset,args.pattern)
    xsecs, nevts = store_info(log_list,args.nproc)
    eval_matching_eff(xsecs, nevts)

